const http = require('http')
const express = require('express') // require express module
const app = new express() // calls express function to start new Express app
const ejs = require('ejs')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload')
const newPostController = require('./controllers/newPost')
const homeController = require('./controllers/home')
const storePostController = require('./controllers/storePost')
const getPostController = require('./controllers/getPost')
const validateMiddleWare = require('./middlware/validationMiddleWare')
const newUserController = require('./controllers/newUser')
const storeUserController = require('./controllers/storeUser')
const loginController = require('./controllers/login')
const loginUserController = require('./controllers/loginUser')
const expressSession = require('express-session')
const authMiddleware = require('./middlware/authMIddleware')
const redirectAuthenticatedMiddleware = require('./middlware/redirectAuthenticatedMiddleware')
const panelController = require('./controllers/panel')
const logoutController = require('./controllers/logout')
const panelMiddleware = require('./middlware/panelMIddleware')
const flash = require('connect-flash')
const newMatchController = require('./controllers/newMatch')
const storeMatchController = require('./controllers/storeMatch')
const newsController = require('./controllers/news')
const calendarController = require('./controllers/calendar')
const newPlayerController = require('./controllers/newplayer')
const storePlayerController = require('./controllers/storePlayer')
const playerController = require('./controllers/players')
const newPersonaController = require('./controllers/newpersona')
const storePersonalController = require('./controllers/storePersonal')
const personaController = require('./controllers/personal')
const mediaController = require('./controllers/media')
const storeMediaController = require('./controllers/storeMedia')
const newAlbumController = require('./controllers/newalbum')
const getAlbumController = require('./controllers/getAlbum')
const getEditPostController = require('./controllers/getEditPost')
const storeEditPostController = require('./controllers/updateStorePost')
const deletePostController = require('./controllers/deletePost')
const getMatchController = require('./controllers/getMatch')
const getEditMatchController = require('./controllers/getEditMatch')
const storeEditMatchController = require('./controllers/updateMatchPost')
const getDocumentationController = require('./controllers/getDocumentation')
const getPlayerController = require('./controllers/getPlayer')
const getEditPlayerController = require('./controllers/getEditPlayer')
const storeEditPlayerController = require('./controllers/updatePlayer')
const deletePlayerController = require('./controllers/deletePlayer')
const getPlayerStatsController = require('./controllers/stats')
const getEditStatsController = require('./controllers/getEditStats')
const getUpdateStatsController = require('./controllers/updateStats')
const getEditMatchesController = require('./controllers/getEditMatches')
const updateMatches = require('./controllers/updateMatches')
app.use(flash())
app.use(expressSession({
    secret: 'keyboard cat'
}))
global.loggedIn = null;
app.use("*", (req, res, next)=>{
    loggedIn = req.session.userId;
    next()
});
app.use(fileUpload())
app.use(express.json())
app.use(express.urlencoded({extended:true}))

mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true})
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.listen(3012,()=> {
    console.log('ok')
})
app.get('/', homeController)
app.get('/post/:id', getPostController)
app.get('/panel',panelMiddleware, panelController )
app.get('/news/new',panelMiddleware, newPostController)
app.post('/posts/store',authMiddleware, storePostController)

/*USER*/
app.get('/auth/register',redirectAuthenticatedMiddleware, newUserController)
app.post('/users/register',redirectAuthenticatedMiddleware,  storeUserController)
app.get('/auth/login',redirectAuthenticatedMiddleware,  loginController);
app.post('/users/login',redirectAuthenticatedMiddleware,  loginUserController)
app.get('/auth/logout', logoutController)
/* */
/*MATCHES*/
app.get('/match/new', panelMiddleware, newMatchController)
app.post('/matches/store', authMiddleware, storeMatchController)
app.get('/match/:id', getMatchController)

/*NEWS*/
app.get('/news', newsController)

app.get('/calendar', calendarController)

/*PLAYERS*/
app.get('/player/new', panelMiddleware, newPlayerController)
app.post('/player/store', storePlayerController)
app.get('/players', playerController)

/*PERSONAL*/
app.get('/persona/new', panelMiddleware, newPersonaController)
app.post('/persona/store', storePersonalController)
app.get('/personal', personaController)

/*MEDIA*/
app.get('/album/new', panelMiddleware, newAlbumController)
app.get('/media', mediaController)
app.post('/media/store', storeMediaController)
app.get('/album/:id', getAlbumController)


/*EDIT POST*/
app.get('/post/edit/:id', getEditPostController)
app.post('/posts/updatestore/:id', storeEditPostController)
app.get('/post/delete/:id', deletePostController)

/*EDIT MATCH*/
app.get('/match/edit/:id', getEditMatchController)
app.post('/match/updatestore/:id', storeEditMatchController)
app.get('/matches/edit', getEditMatchesController)

/*EDIT PLAYERS*/
app.get('/player/:id', getPlayerController)
app.get('/player/edit/:id', getEditPlayerController)
app.post('/player/updatestore/:id', storeEditPlayerController)
app.get('/player/delete/:id', deletePlayerController)
app.post('/matches/update/:id', updateMatches)
/*PLAYER STATS*/
app.get('/stats', getPlayerStatsController)
app.get('/stats/edit', getEditStatsController)
app.post('/stats/update/:id', getUpdateStatsController)

app.get('/documentation',panelMiddleware, getDocumentationController)

app.use((req, res)=> res.render('notfound'));