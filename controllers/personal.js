const Persona = require('../models/Persona')

module.exports = async(req, res) => {
    const personal = await Persona.find({}).where({"position" : "Руководство"}).sort({role: 1})
    const personal2 = await Persona.find({}).where({"position" : "Тренерский штаб"})
    res.render('personal', {
        personal, personal2
    });
}