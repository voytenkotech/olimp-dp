const Media = require('../models/Media')

module.exports = async(req, res) => {
    const media = await Media.find({})
    res.render('media', {
        media,
    });
}