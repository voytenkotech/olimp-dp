const BlogPost = require('../models/BlogPost')
const moment = require('moment')
moment.locale('ru')
module.exports = async(req, res) => {
    function limitStr(str, n, symb) {
        if (!n && !symb) return str;
        symb = symb || '...';
        return str.substr(0, n - symb.length) + symb;
    }
    const blogposts = await BlogPost.find({})
    res.render('news', {
        blogposts, limitStr, moment
    });
}