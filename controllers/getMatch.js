const MatchPost = require('../models/MatchPost')

module.exports = async(req, res) => {
    const matchpost = await MatchPost.findById(req.params.id)
    const match_id = matchpost.id
    res.render('match', {
        matchpost, match_id
    });
}