const BlogPost = require('../models/BlogPost')

module.exports = async(req,res) => {
    if(req.session.userId){
        const id = req.url
        const post_id = id.slice(11);
        const post = await BlogPost.findById(post_id)
        return res.render('postedit',{
            createPost: true,
            post, post_id
        });
    }
    res.redirect('/auth/login')
}
