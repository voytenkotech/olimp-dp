module.exports = async(req,res) => {
    if(req.session.userId){

        return res.render('documentation');
    }
    res.redirect('/auth/login')
}
