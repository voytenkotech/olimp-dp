const Media = require('../models/Media')

module.exports = async(req, res) => {

    const album = await Media.findById(req.params.id)
    console.log(album)
    res.render('album', {
        album
    });
}