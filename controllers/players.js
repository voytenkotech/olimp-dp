const Player = require('../models/Player')

module.exports = async(req, res) => {
    const goalkeepers = await Player.find({}).where({"role" : 'Вратарь'}).sort({"number" : 1})
    const quarterbacks = await Player.find({}).where({"role" : 'Защитник'}).sort({"number" : 1})
    const midfielders = await Player.find({}).where({"role" : 'Полузащитник'}).sort({"number" : 1})
    const attackers = await Player.find({}).where({"role" : "Нападающий"}).sort({"number" : 1})
    res.render('players', {
        goalkeepers, quarterbacks, midfielders, attackers
    });
}