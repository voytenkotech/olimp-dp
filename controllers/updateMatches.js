const MatchPost = require('../models/MatchPost')
const { SMTPClient } = require('emailjs')

module.exports = async (req, res) => {
    const id = req.url
    const match_id = id.slice(16);
    const match = MatchPost.findById(match_id)
    const client = new SMTPClient({
      user: "rugby@signal-tv.com",
      password: "msjnplqzotnfgwss",
      host: "smtp.yandex.ru",
      ssl: true,
    });
  
    client.send(
      {
        text: `Обновили матч Город: ${req.body.city}, Дата: ${req.body.date}, Счёт: ${req.body.score}, Время: ${req.body.time}. ${match_id}`,
        from: "rugby@signal-tv.com",
        to: "voytenkodev@yandex.ru",
        subject: "Обновление матча",
      },
      (err, message) => {
        console.log(err || message);
      }
    );
    await match.update({
      ...req.body,
    });
    res.redirect("/matches/edit");
};