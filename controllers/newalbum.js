module.exports = async (req, res) => {
    if(req.session.userId){
        return res.render('newalbum');
    }
    res.redirect('/auth/login')
}