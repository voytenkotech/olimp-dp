const BlogPost = require('../models/BlogPost')
const path = require('path')

module.exports = async(req, res)=> {
    if (req.files == null) {
        await BlogPost.create({
            ...req.body,
            image: '/img/news/logo.png'
        })
        res.redirect('/')
    } else {
        let image = req.files.image
        image.mv(path.resolve(__dirname,'..','public/img/news/',image.name),
        async(error)=>{
        await BlogPost.create({
            ...req.body,
            image: '/img/news/' + image.name
        })
        res.redirect('/')
    })
    }
}