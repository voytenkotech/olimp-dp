const Player = require('../models/Player')
const path = require('path')

module.exports = async (req, res) => {
    const id = req.url
    const player_id = id.slice(20);
    const player = Player.findById(player_id)
    if (req.files == null) {
      await player.update({
          ...req.body,
          image: player.image
      })
      res.redirect('/players')
  } else {
      let image = req.files.image
      image.mv(path.resolve(__dirname,'..','public/img/players/',image.name),
      async(error)=>{
      await player.update({
          ...req.body,
          image: '/img/players/' + image.name
      })
      res.redirect('/players')
  })
  }
}