const MatchPost = require('../models/MatchPost')

module.exports = async(req, res) => {
    const matchposts = await MatchPost.find({}).where({"tour": 1});
    const matchposts2 = await MatchPost.find({}).where({"tour": 2});
    const matchposts3 = await MatchPost.find({}).where({"tour": 3});
    const matchposts4 = await MatchPost.find({}).where({"tour": 4});
    const matchposts5 = await MatchPost.find({}).where({"tour": 5});
    const matchposts6 = await MatchPost.find({}).where({"tour": 6});
    const matchposts7 = await MatchPost.find({}).where({"tour": 7});
    const matchposts8 = await MatchPost.find({}).where({"tour": 8});
    const matchposts9 = await MatchPost.find({}).where({"tour": 9});
    const matchposts10 = await MatchPost.find({}).where({"tour": 10});
    const matchposts11 = await MatchPost.find({}).where({"tour": 11});
    const matchposts12 = await MatchPost.find({}).where({"tour": 12});
    const matchposts13 = await MatchPost.find({}).where({"tour": 13});
    const matchposts14 = await MatchPost.find({}).where({"tour": 14});
    const matchposts15 = await MatchPost.find({}).where({"tour": 15});
    const matchposts16 = await MatchPost.find({}).where({"tour": 16});
    const matchposts17 = await MatchPost.find({}).where({"tour": 17});
    const matchposts18 = await MatchPost.find({}).where({"tour": 18});
    const matchposts19 = await MatchPost.find({}).where({"tour": 19});
    const matchposts20 = await MatchPost.find({}).where({"tour": 20});
    const matchposts21 = await MatchPost.find({}).where({"tour": 21});
    const matchposts22 = await MatchPost.find({}).where({"tour": 22});
    const matchposts23 = await MatchPost.find({}).where({"tour": 23});
    const matchposts24 = await MatchPost.find({}).where({"tour": 24});
    const matchposts25 = await MatchPost.find({}).where({"tour": 25});
    const matchposts26 = await MatchPost.find({}).where({"tour": 26});
    const matchposts27 = await MatchPost.find({}).where({"tour": 27});
    const matchposts28 = await MatchPost.find({}).where({"tour": 28});
    const matchposts29= await MatchPost.find({}).where({"tour": 29});
    const matchposts30 = await MatchPost.find({}).where({"tour": 30});
    const matchposts31 = await MatchPost.find({}).where({"tour": 31});
    const matchposts32 = await MatchPost.find({}).where({"tour": 32});
    const matchposts33 = await MatchPost.find({}).where({"tour": 33});
    const matchposts34 = await MatchPost.find({}).where({"tour": 34});
    const matchposts35 = await MatchPost.find({}).where({"tour": 35});
    const matchposts36 = await MatchPost.find({}).where({"tour": 36});
    const matchposts37 = await MatchPost.find({}).where({"tour": 37});
    const matchposts38 = await MatchPost.find({}).where({"tour": 38});

    var date = new Date()
    var month = date.getMonth() + 1
    var day = date.getDate() -1
    var year = date.getFullYear()
    var monthtostring = month.toString()
    if (monthtostring.length < 2){
        month = '0' + monthtostring
    }
    var daytostring = day.toString()
    if (daytostring.length != 2){
        day = '0' + daytostring
    }
    var currentdate =  year + '.' + month + '.' + day

    res.render('calendar', {
         matchposts, matchposts2, matchposts3, matchposts4, matchposts5,
         matchposts6, matchposts7, matchposts8, matchposts9, matchposts10,
         matchposts11, matchposts12, matchposts13, matchposts14, matchposts15, matchposts16, 
         matchposts17, matchposts18, matchposts19, matchposts20, matchposts21, matchposts22,
         matchposts23, matchposts24, matchposts25, matchposts26, matchposts27, matchposts28,
         matchposts29, matchposts30, matchposts31, matchposts32, matchposts33, matchposts34, 
         matchposts35, matchposts36, matchposts37, matchposts38, currentdate
    });
}