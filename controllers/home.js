const BlogPost = require('../models/BlogPost')
const MatchPost = require('../models/MatchPost')

const moment = require('moment')
moment.locale('ru')
module.exports = async(req, res) => {
    const matchposts = await MatchPost.find({});
    var date = new Date()
    var month = date.getMonth() + 1
    var day = date.getDate() -1
    var year = date.getFullYear()
    var monthtostring = month.toString()
    if (monthtostring.length < 2){
        month = '0' + monthtostring
    }
    var daytostring = day.toString()
    if (daytostring.length != 2){
        day = '0' + daytostring
    }
    var currentdate =  year + '.' + month + '.' + day
    function limitStr(str, n, symb) {
        if (!n && !symb) return str;
        symb = symb || '...';
        return str.substr(0, n - symb.length) + symb;
    }
    
    const blogposts = await BlogPost.find({})
    res.render('index', {
        blogposts, matchposts, currentdate, limitStr, moment
    });
}