const MatchPost = require('../models/MatchPost')

module.exports = async(req, res) => {
    if(req.session.userId){
        const matches = await MatchPost.find({}).sort({tour: "1"})
        res.render('editmatches', {
            matches
        });
    }
}