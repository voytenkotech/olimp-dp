const Persona = require("../models/Persona");
const path = require('path')

module.exports = (req, res)=> {
    let image = req.files.image;
    image.mv(path.resolve(__dirname,'..','public/img/personal/',image.name),
    async(error)=>{
        await Persona.create({
            ...req.body,
            image: '/img/personal/' + image.name
        })
        res.redirect('/personal')
    })
}