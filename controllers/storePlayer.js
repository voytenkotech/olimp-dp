const Player = require("../models/Player");
const path = require('path');

module.exports = async(req, res)=> {
    if (req.files == null || req.files == undefined) {
        await Player.create({
            ...req.body,
            image: '/img/players/man.png'
        })
        res.redirect('/players')
    } else {
        let image = req.files.image;
        image.mv(path.resolve(__dirname,'..','public/img/players/',image.name),
        async(error)=>{
            await Player.create({
                ...req.body,
                image: '/img/players/' + image.name
            })
            res.redirect('/players')
        })
    }
}