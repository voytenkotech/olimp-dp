const Player = require('../models/Player')

module.exports = async(req, res) => {
    const player = await Player.findById(req.params.id)
    console.log(player)
    const player_id = player.id
    console.log(player_id)
    res.render('player', {
        player, player_id
    });
}