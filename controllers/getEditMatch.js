const MatchPost = require('../models/MatchPost')

module.exports = async(req,res) => {
    if(req.session.userId){
        const id = req.url
        const match_id = id.slice(12);
        const match = await MatchPost.findById(match_id)
        return res.render('matchedit',{
            match, match_id
        });
    }
    res.redirect('/auth/login')
}
