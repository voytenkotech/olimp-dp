const BlogPost = require('../models/BlogPost')
const moment = require('moment')
moment.locale('ru')
module.exports = async(req, res) => {
    const blogpost = await BlogPost.findById(req.params.id)
    const post_id = blogpost.id
    res.render('post', {
        blogpost, post_id, moment
    });
}