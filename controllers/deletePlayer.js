const Player = require('../models/Player')
const path = require('path')

module.exports = async(req, res)=> {
    const id = req.url
    const player_id = id.slice(15);
    const player = Player.findById(player_id)
    await player.deleteMany()
    res.redirect('/players')
}