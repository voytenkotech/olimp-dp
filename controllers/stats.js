const Player = require('../models/Player')

module.exports = async(req, res) => {
    const players = await Player.find({}).sort({"name": 1})
    res.render('playerstats', {
        players
    });
}