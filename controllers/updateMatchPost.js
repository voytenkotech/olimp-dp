const MatchPost = require('../models/MatchPost')


module.exports = async (req, res) => {
    const id = req.url
    const match_id = id.slice(19);
    const match = MatchPost.findById(match_id)
    await match.update({
      ...req.body,
    });
    res.redirect("/");
  };