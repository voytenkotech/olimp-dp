const Player = require('../models/Player')

module.exports = async(req,res) => {
    if(req.session.userId){
        const id = req.url
        const player_id = id.slice(13);
        const player = await Player.findById(player_id)
        return res.render('playeredit',{
            player, player_id
        });
    }
    res.redirect('/auth/login')
}
