module.exports = async (req, res) => {
    if(req.session.userId){
        return res.render('newpersona');
    }
    res.redirect('/auth/login')
}