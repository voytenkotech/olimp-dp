const Player = require('../models/Player')

module.exports = async(req, res) => {
    if(req.session.userId){
        const players = await Player.find({}).sort({"name": 1})
        res.render('editstats', {
            players
        });
    }
}