const BlogPost = require('../models/BlogPost')
const path = require('path')

module.exports = async(req, res)=> {
    const id = req.url
    const post_id = id.slice(13);
    const post = BlogPost.findById(post_id)
    await post.deleteMany()
    res.redirect('/')
}