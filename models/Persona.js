const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const PersonaSchema = new Schema({
    name: String,
    role: String,
    date: String,
    image: String,
    position: String, 
});

const Persona = mongoose.model('Persona', PersonaSchema)

module.exports = Persona