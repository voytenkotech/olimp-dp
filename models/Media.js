const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const MediaSchema = new Schema({
    number: {
        type: String,
        default: ' '
    },
    name: {
        type: String,
        default: ' '
    },
    images: {
        type: Array,
        default: 'none.jpg',
    }
});

const Media = mongoose.model('Media', MediaSchema)

module.exports = Media