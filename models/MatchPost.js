const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const MatchPostSchema = new Schema({
    date: {
        type: String,
        default: '-'
    },
    city: {
        type: String,
        default: '-'
    },
    stadium: {
        type: String,
        default: '-'
    },
    team_1: {
        type: String,
        default: '-'
    },
    team_2: {
        type: String,
        default: '-'
    },
    time: {
        type: String,
        default: '-'
    },
    score: {
        type: String,
        default: '-/-'
    },
    tour: {
        type: String,
        default: '-'
    },
    tournament: {
        type: String,
        default: '-'
    }
});

const MatchPost = mongoose.model('MatchPost', MatchPostSchema);

module.exports = MatchPost