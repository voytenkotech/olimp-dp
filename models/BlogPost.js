const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const BlogPostSchema = new Schema({
    title: {
        type: String,
        default: ''
    },
    keywords: {
        type: String,
        default: ''
    },
    body: {
        type: String,
        default: ''
    },
    image: {
        type: String,
        default: '/img/news/logo.png'
    },
    datePosted: {
        type: String,
        default: Date.now
    }
});
 
const BlogPost = mongoose.model('BlogPost', BlogPostSchema)

module.exports = BlogPost