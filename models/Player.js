const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const PlayerSchema = new Schema({
    name: String,
    number: Number,
    role: String,
    date: String,
    image: String,
    citizenship: String,
    height: String,
    weight: String,
    games: {
        type: Number,
        default: 0
    },
    time: {
        type: Number,
        default: 0
    },
    wasreplaced: {
        type: Number,
        default: 0
    },
    replaced: {
        type: Number,
        default: 0
    },
    goalassist: {
        type: Number,
        default: 0
    },
    goals: {
        type: Number,
        default: 0
    },
    yellowcard: {
        type: Number,
        default: 0
    },
    redcard: {
        type: Number,
        default: 0
    },
});

const Player = mongoose.model('Player', PlayerSchema)

module.exports = Player